from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from accounts.forms import LoginForm, SignupForm
from django.contrib.auth.models import User


# Create your views here.
def user_login(request):
    if request.method == "POST":
        login_form = LoginForm(request.POST)

        if login_form.is_valid():
            username = login_form.cleaned_data["username"]
            password = login_form.cleaned_data["password"]
            user = authenticate(
                request,
                username=username,
                password=password,
            )

            if user is not None:
                login(request, user)
                return redirect("list_projects")

    else:
        login_form = LoginForm()

    context = {
        "form": login_form,
    }

    return render(request, "accounts/login.html", context)


def user_logout(request):
    logout(request)
    return redirect("login")


def user_signup(request):
    if request.method == "POST":
        signup_form = SignupForm(request.POST)

        if signup_form.is_valid():
            username = signup_form.cleaned_data["username"]
            password = signup_form.cleaned_data["password"]
            password_confirmation = signup_form.cleaned_data[
                "password_confirmation"
            ]

            if password == password_confirmation:
                new_user = User.objects.create_user(
                    username,
                    password=password,
                )
                login(request, new_user)
                return redirect("list_projects")

            else:
                signup_form.add_error("password", "the passwords do not match")

    else:
        signup_form = SignupForm()

    context = {
        "form": signup_form,
    }

    return render(request, "accounts/signup.html", context)
