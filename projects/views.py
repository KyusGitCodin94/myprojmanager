from django.shortcuts import render, redirect, get_object_or_404
from projects.models import Project
from django.contrib.auth.decorators import login_required
from projects.forms import NewProjectForm


# Create your views here.
@login_required
def projects_list(request):
    projects_list = Project.objects.filter(owner=request.user)
    context = {"projects": projects_list}
    return render(request, "projects/projects_list.html", context)


@login_required
def project_details(request, id):
    selected_project = get_object_or_404(Project, id=id)
    context = {
        "project": selected_project,
    }
    return render(request, "projects/project_details.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        new_project_form = NewProjectForm(request.POST)
        if new_project_form.is_valid():
            new_project_form.save()
            return redirect("list_projects")
    else:
        new_project_form = NewProjectForm()
    context = {
        "form": new_project_form,
    }
    return render(request, "projects/create_project.html", context)
