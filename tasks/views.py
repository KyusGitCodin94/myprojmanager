from django.shortcuts import render, redirect
from tasks.models import Task
from django.contrib.auth.decorators import login_required
from tasks.forms import NewTaskForm


# Create your views here.
@login_required
def create_task(request):
    if request.method == "POST":
        new_task_form = NewTaskForm(request.POST)
        if new_task_form.is_valid():
            new_task_form.save()
            return redirect("list_projects")
    else:
        new_task_form = NewTaskForm()
    context = {
        "form": new_task_form,
    }
    return render(request, "tasks/create_task.html", context)


@login_required
def tasks_list(request):
    tasks_list = Task.objects.filter(assignee=request.user)
    context = {
        "tasks": tasks_list,
    }
    return render(request, "tasks/tasks_list.html", context)
